module.exports = {
    ci: {
        collect: {
            puppeteerScript: './puppeteerScript.js',
            puppeteerLaunchOptions: {args: ['--allow-no-sandbox-job', '--allow-sandbox-debugging', '--no-sandbox', '--disable-gpu', '--disable-gpu-sandbox', '--display']}, //https://www.puppeteersharp.com/api/PuppeteerSharp.LaunchOptions.html
            numberOfRuns: 1,
            disableStorageReset: true,
            settings: {
                // Don't clear localStorage/IndexedDB/etc before loading the page.
                "disableStorageReset": true,
                // Wait up to 60s for the page to load
                "maxWaitForLoad": 60000,
                // Use applied throttling instead of simulated throttling
                "throttlingMethod": "devtools"
            },
            url: ['https://frappfrontrec.z28.web.core.windows.net/login', 'https://frappfrontrec.z28.web.core.windows.net/entities', 'https://frappfrontrec.z28.web.core.windows.net/']
        },
        upload: {
            target: 'filesystem',
            outputDir: './lhci_reports',
            reportFilenamePattern: '%%PATHNAME%%-%%DATETIME%%-report.%%EXTENSION%%'
            // token: '',
            // serverBaseUrl: ''
        },
        assert: {
            "assertions": {
                "categories:performance": ["error", {"minScore": 0.8}],
                "categories:accessibility": ["error", {"minScore": 0.8}],
                "categories:best-practices": ["error", {"minScore": 0.8}],
                "categories:PWA": ["error", {"minScore": 0.4}]?
                "categories:seo": ["error", {"minScore": 0.5}]
            }
        },
    },
};